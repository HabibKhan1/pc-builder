import Parts
from time import sleep

sockets = ["am4", "lga1700"]
cost = 0

def help():
    try:
        print("You're in the PC builder guide.\n")

        sleep(2)
    
        print("First of all, a PC consists of a few basic components:\n")

        sleep(2)
    
        print("CPU")
        print("GPU")
        print("RAM")
        print("Storage")
        print("Motherboard")
        print("PSU")
        print("PC case")
        print("Sound card")
        print("Wi-Fi adapter\n")

        sleep(20)

        print("The CPU is VERY IMPORTANT. If it's missing, your PC won't function. It processes information.")
        print("Some CPUs have integrated graphics, which is almost like a built-in video card. If you have this kind of CPU, it takes care of the graphics for you.")
        print("CPUs with integrated graphics tend to be better for budget builds.")
        print("Separate GPUs, on the other hand, might be good for certain graphical functions, but integrated graphics are very usable.\n")

        sleep(15)

        print("RAM is also very essential. If you don't have any, your PC wouldn't be able to have memory to do anything or display anything.\n")

        sleep(10)

        print("Storage, as you may have guessed, holds your files.")
        print("There are two types of storage devices: HDDs (hard drives) and SSDs.")
        print("You will usually want to go for an SSD. HDDs are outdated.\n")

        sleep(20)

        print("The motherboard holds together a lot of the important stuff.")
        print("It has slots for the CPU, RAM, storage device, and GPU.\n")

        sleep(15)

        print("The PSU gives the PC a steady supply of power.")
        print("It's always plugged into a wall socket with an AC adapter, meaning that PCs NEVER need charge (unlike laptops.)")
        print("However, for the record, PCs are NOT very portable (unlike laptops), meaning they work best in one place.\n")

        sleep(20)

        print("The PC case, as you may have guessed, is the case for the PC which everything gets installed in.")
        print("The PSU and motherboard get all wired up and installed in the case.\n")

        sleep(15)

        print("The sound card is for audio. However, your case might have an audio cable which you can install in the motherboard.")
        print("In that case, you can get a headset. A headset not only provides audio OUTPUT, it also has a microphone for INPUT.\n")

        sleep(15)

        print("A Wi-Fi adapter is absolutely necessary (unless your motherboard supplies Wi-Fi antennae) to connect to a wireless network.")
        print("For wired networks, chances are your motherboard may have an Ethernet cable, or you can get appropriate hardware for that purpose.\n")

        sleep(15)

        print("On to building the PC!\n")

        sleep(2)

        print("PCPartPicker is a very good website for building a PC. You can save part lists and buy parts from, say, Newegg and Amazon.")
        print("It also has compatibility filters to make sure parts are NOT incompatible, since your PC parts will probably all be from different brands.\n")

        sleep(15)

        print("It's a good idea to start with choosing the motherboard first, since, as always, you want to make sure the other parts will be compatible with it.\n")

        sleep(10)

        print("After that, you can get the other parts, such as the CPU, GPU, RAM, etc.\n")

        sleep(10)

        print("The procedure for building a PC will probably vary depending on which brands you use. However, you can follow the manuals your vendors provide.\n")
    except KeyboardInterrupt:
        print("Exiting PC builder guide....")

        return

print("Welcome to the PC builder!")
guide = input("Would you like to open the PC builder guide? Y/N ")

if guide.lower() == "y":
    help()
    
socket = input("Which socket? AM4 or LGA1700? ")

while socket.lower() not in sockets:
    try:
        print("Invalid socket!")
        socket = input("Which socket? AM4 or LGA1700? ")
    except KeyboardInterrupt:
        print("Exiting....")
        
cpu_series = input('Which CPU series? (Type "L" to list them all.) ')

while cpu_series.lower() == "l":
    try:
        print("Available options for CPU series selection:\n")

        if socket.lower() == "am4":
            for x in Parts.AM4:
                print(x)
        else: # Must be LGA1700
            for x in Parts.LGA1700:
                print(x)

        cpu_series = input('Which CPU series? (Type "L" to list them all.) ')
    except KeyboardInterrupt:
        print("Exiting....")

        exit()

cost += Parts.cpu_model(cpu_series, socket)
cost += Parts.get_gpu()
cost += Parts.get_motherboard()

print(f'The total cost is: ${format(cost, ".2f")}.')
